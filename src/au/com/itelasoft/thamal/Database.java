package au.com.itelasoft.thamal;

import java.sql.*;

public class Database {

    private static String url = "jdbc:mysql://localhost:3306/javafundamentals";
    private static String userName = "root";
    private static String password = "thamal.123";

    private static Connection connection;

    // Singleton connection instance
    public static Connection getConnection() throws SQLException {
        if (connection == null) {
            connection = DriverManager.getConnection(url, userName, password);
        }
        return connection;
    }

    // Retrieve all Employees
    public static ResultSet getAllEmployees() throws SQLException {
        String query = "SELECT * FROM employee";
        ResultSet resultSet = getConnection().createStatement().executeQuery(query);
        return resultSet;
    }

    // Get employee by NIC
    public static ResultSet getEmployee(String nic) throws SQLException {
        String query = "SELECT * FROM employee WHERE nic = ?";
        PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, nic);
        ResultSet resultSet = preparedStatement.executeQuery();
        return resultSet;
    }

    // Insert Employee
    public static boolean insertEmployee(Employee employee) {

        try {

            String query = "INSERT INTO employee(first_name, second_name, phone_number, nic, email, age, job_role, job_location)" +
                    "VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
            PreparedStatement preparedStatement = getConnection().prepareStatement(query);
            preparedStatement.setString(1, employee.getFirstName());
            preparedStatement.setString(2, employee.getSecondName());
            preparedStatement.setString(3, employee.getPhoneNumber());
            preparedStatement.setString(4, employee.getNic());
            preparedStatement.setString(5, employee.getEmail());
            preparedStatement.setInt(6, employee.getAge());
            preparedStatement.setString(7, employee.getJobRole());
            preparedStatement.setString(8, employee.getJobLocation());

            // Return boolean status if the table updated
            if (preparedStatement.executeUpdate() != 0) {
                return true;
            } else {
                return false;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return false;
        }
    }

    // Update Employee
    public static boolean updateEmployee(String nic, String email) throws SQLException {
        try {
            String query = "UPDATE employee SET email = ? WHERE nic = ?";
            PreparedStatement preparedStatement = getConnection().prepareStatement(query);
            preparedStatement.setString(1, email);
            preparedStatement.setString(2, nic);

            // Return boolean status if the table updated
            if (preparedStatement.executeUpdate() != 0) {
                return true;
            } else {
                return false;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return false;
        }
    }

    // Delete Employee
    public static boolean deleteEmployee(String nic) {
        try {
            String query = "DELETE FROM employee WHERE nic = ?";
            PreparedStatement preparedStatement = getConnection().prepareStatement(query);
            preparedStatement.setString(1, nic);

            // Return boolean status if the table updated
            if (preparedStatement.executeUpdate() != 0) {
                return true;
            } else {
                return false;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return false;
        }
    }
}
