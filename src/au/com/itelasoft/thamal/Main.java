package au.com.itelasoft.thamal;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws Exception {

        EmployeeController employeeController = new EmployeeController();
        Scanner scanner = new Scanner(System.in);

        boolean quit = false;
        char selection;

        while (quit == false) {

            // Prompt the main menu
            System.out.println("\n---- Main Menu ----");
            System.out.println("1 - Add Employee");
            System.out.println("2 - View Employee");
            System.out.println("3 - Delete Employee");
            System.out.println("4 - Update Employee");
            System.out.println("q - Quite");

            // Prompt and reading the user input
            System.out.print("\nEnter the Menu Number: ");
            selection = scanner.next().charAt(0);

            switch (selection) {
                case '1': // Insert a new employee

                    System.out.println("\nEnter the following details to add new employee\n");

                    // Build a new Employee instance with user inputs
                    Employee newEmployee = new Employee();
                    newEmployee.setFirstName(getStringReadings("First Name", false));
                    newEmployee.setSecondName(getStringReadings("Second Name", false));
                    newEmployee.setNic(getStringReadings("NIC", true));
                    newEmployee.setPhoneNumber(getStringReadings("Phone Number", false));
                    newEmployee.setEmail(getStringReadings("Email", true));
                    newEmployee.setAge(getIntReadings("Age"));
                    newEmployee.setJobRole(getStringReadings("Job Role", false));
                    newEmployee.setJobLocation(getStringReadings("Job Location", false));

                    employeeController.insertEmployee(newEmployee);
                    break;

                case '2': // Display all the employees
                    System.out.println();
                    System.out.format("%-15s%-15s%-15s%-40s%-10s%-35s%-20s%-10s\n",
                            "First Name", "NIC", "Phone Number", "Email", "Age", "Job Role", "Job Location", "ID");

                    for (Employee employee : employeeController.getAllEmployees()) {
                        System.out.format("%-15s%-15s%-15s%-40s%-10d%-35s%-20s%-10d\n",
                                employee.getFirstName(), employee.getNic(), employee.getPhoneNumber(), employee.getEmail(),
                                employee.getAge(), employee.getJobRole(), employee.getJobLocation(), employee.getEmpID());
                    }
                    break;

                case '3': // Delete an employee
                    System.out.print("Please enter the employee's ");
                    employeeController.deleteEmployee(getStringReadings("NIC", false));
                    break;

                case '4': // Update an employee
                    System.out.print("Please enter the employee's ");
                    String nic = getStringReadings("NIC", true);

                    Employee employee = employeeController.getEmployee(nic);

                    if(employee.getFirstName() != null) {
                        displayEmployee(employee);

                        System.out.print("Enter the new email: ");
                        String email = getStringReadings("Email", true);
                        employeeController.updateEmployee(nic ,email);

                        employee = employeeController.getEmployee(nic);
                        displayEmployee(employee);

                    } else {
                        System.out.println("Couldn't fetch data for given NIC. Please check and try again!");
                    }

                    break;

                case 'q':
                case 'Q':
                    System.out.println("Good Bye!");
                    quit = true;
                    break;

                default:
                    System.out.println("You entered invalid menu number. Please try again!");
                    break;
            }
        }
    }

    // Prompt user to get valid string input
    private static String getStringReadings(String label, boolean validate) {

        Scanner scanner = new Scanner(System.in);
        String input;

        do {
            System.out.print(label + ": ");
            input = scanner.next();

            if (input.isEmpty()) {
                System.out.println(label + " is required!");
                continue;
            }

            if (validate) {
                if (label.equals("Email") && Validator.isValidEmail(input)) {
                    break;
                } else if (label.equals("NIC") && Validator.isValidNic(input)) {
                    break;
                } else {
                    System.out.println("Invalid Input! Please check and try again.");
                    input = "";
                }
            }
        } while (input.isEmpty());

        return input;
    }

    // Prompt user to get valid integer input
    private static int getIntReadings(String label) {
        Scanner scanner = new Scanner(System.in);
        int input;

        while (true) {
            try {
                System.out.print(label + ": ");
                input = scanner.nextInt();
                break;
            } catch (InputMismatchException exception) {
                System.out.println("Invalid input! Please check and try again.");
                scanner.next();
                continue;
            }
        }
        return input;
    }

    // Display employee details
    private static void displayEmployee(Employee employee) {
        if(employee.getFirstName() != null) {
            System.out.format("%-15s%-15s%-15s%-40s%-10s%-35s%-20s%-10s\n",
                    "First Name", "NIC", "Phone Number", "Email", "Age", "Job Role", "Job Location", "ID");

            System.out.format("%-15s%-15s%-15s%-40s%-10d%-35s%-20s%-10d\n\n",
                    employee.getFirstName(), employee.getNic(), employee.getPhoneNumber(), employee.getEmail(),
                    employee.getAge(), employee.getJobRole(), employee.getJobLocation(), employee.getEmpID());

        } else {
            System.out.println("Couldn't fetch data for given NIC. Please check and try again!");
        }
    }
}
