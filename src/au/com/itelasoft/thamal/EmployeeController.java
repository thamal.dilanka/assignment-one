package au.com.itelasoft.thamal;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class EmployeeController {

    // Retrieve all the employees from the table
    public ArrayList<Employee> getAllEmployees() {

        try {
            ResultSet resultSet = Database.getAllEmployees();
            ArrayList<Employee> employees = new ArrayList<>();

            while (resultSet.next()) {
                Employee employee = new Employee();

                // Setting attributes to the employee instance
                employee.setFirstName(resultSet.getString("first_name"));
                employee.setNic(resultSet.getString("nic"));
                employee.setAge(resultSet.getInt("age"));
                employee.setEmail(resultSet.getString("email"));
                employee.setPhoneNumber(resultSet.getString("phone_number"));
                employee.setJobRole(resultSet.getString("job_role"));
                employee.setJobLocation(resultSet.getString("job_location"));
                employee.setEmpID(resultSet.getInt("emp_id"));

                employees.add(employee);
            }

            return employees;

        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return null;
        }
    }

    // Retrieve specific employee from the table
    public  Employee getEmployee(String nic) {
        try {
            ResultSet resultSet = Database.getEmployee(nic);

            Employee employee = new Employee();

            while (resultSet.next()) {
                // Setting attributes to the employee instance
                employee.setFirstName(resultSet.getString("first_name"));
                employee.setNic(resultSet.getString("nic"));
                employee.setAge(resultSet.getInt("age"));
                employee.setEmail(resultSet.getString("email"));
                employee.setPhoneNumber(resultSet.getString("phone_number"));
                employee.setJobRole(resultSet.getString("job_role"));
                employee.setJobLocation(resultSet.getString("job_location"));
                employee.setEmpID(resultSet.getInt("emp_id"));
            }
            return employee;

        } catch (SQLException throwables) {
            return null;
        }
    }

    // Insert an employee in to the table
    public void insertEmployee(Employee employee) {

        if(Database.insertEmployee(employee)) {
            System.out.println("New employee inserted successfully!");
        } else {
            System.out.println("Something went wrong! Please try again.");
        }
    }

    // Updating an employee
    public Employee updateEmployee(String nic, String email) throws SQLException {

        if(Database.updateEmployee(nic, email)) {
            System.out.println("Update Successful!");
            return getEmployee(nic);
        } else {
            System.out.println("Something went wrong! Please check and try again.");
        }
        return null;
    }

    public void deleteEmployee(String nic) {

        if(Database.deleteEmployee(nic)) {
            System.out.println("Deletion Successful!");
        } else {
            System.out.println("Couldn't fetch data for given NIC. Please check and try again!");
        }
    }
}
