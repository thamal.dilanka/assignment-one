package au.com.itelasoft.thamal;

import java.util.regex.Pattern;

public class Validator {

    // Validating the Email
    public static boolean isValidEmail(String email) {
        Pattern pattern = Pattern.compile("^(.+)@(.+)$");
        return pattern.matcher(email).matches();
    }

    // Validating the NIC
    public static boolean isValidNic(String nic) {
        Pattern newPattern = Pattern.compile("^[0-9]{12}$");
        Pattern oldPattern = Pattern.compile("^[0-9V]{10}$");
        return newPattern.matcher(nic).matches() || oldPattern.matcher(nic).matches();
    }
}
